# bench.py setup

code for [
Accelerating TensorFlow using Apple M1 Max?](https://discuss.tensorflow.org/t/accelerating-tensorflow-using-apple-m1-max/5282/71?u=wildfluss)


## Results so far (see post above)

43m = on my late 2019 intel macbook pro with 16 G of RAM 

46 sec (!) = on a colab GPU A100 High RAM for Pro subscription of ~ $ 10 monthly 

1m 11.1s = on a Mac Studio with M1 Max with 24-core GPU and systemMemory 64.00 GB it “executed in 1m 11.1s” but without ModelCheckpoint callback because that failed (it would not contribute meaningfully anyways)

232.44 s = 3.8m on Macbook Air M1 7 GPU with 16 G of RAM 

## Run this to benchmark on your box:

```bash
tar jxf cats_vs_dogs_small.tbz2  && \
python3 -m venv ~/venv-metal && \
source ~/venv-metal/bin/activate && \
python -m pip install -U pip && \
python -m pip install tensorflow && \
python -m pip install tensorflow-metal && \
python bench.py
```

will output time it took for 30 epochs like this
 
```
val_accuracy: 0.5030
45.74570274353027
```
